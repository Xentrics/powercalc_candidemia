FROM rocker/verse:4.0.3

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Europe/Berlin

WORKDIR /build
RUN apt-get update && \
    apt-get install -y \
            make \
            openssh-server \
            docker.io \
            python3-pip \
            curl \
            zip \
            unzip

RUN usermod -aG sudo rstudio
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY src/install src/install/
RUN Rscript src/install/install.R

SHELL ["/bin/bash", "-c"]
EXPOSE 8787
WORKDIR /analysis
CMD make analyse
