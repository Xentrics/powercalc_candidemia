# Power Calculation of 16S in Candidemia

Power calculation of Candidemia cohort, [Zhai et al, Nature medicine 2020](https://doi.org/10.1038/s41591-019-0709-7)

![MIT License](https://img.shields.io/badge/license-MIT-blue)

<br><br><br>



# HMP Method

- model bacterial profiles using Dirichlet-multinomial distribution (DM)
- with just 15 samples per group, statistical power is greater than 80%
- at 20 samples, power is 100%

<br><br>

![](results/HMP_power_DM.png)

---

<br><br><br><br>



# Micropower Method

- this analysis was heavily guided by [this blog](https://medium.com/brown-compbiocore/power-analyses-for-microbiome-studies-with-micropower-8ff28b36dfe3)
- here, the mean within-group and within-SD must be estimated from an existing cohort
- these parameters are then used to create simulated OTU tables _with the same properties_

<br><br>

- First, rarefaction must be determined
- subsampling influences the within-group **mean**
- Target means were **outside** the simulated data

![](results/mp_hash_mean_tbl.png)

<br>


- in log10 space of mean, we see a **sigmod** curve
- these can be fit well using a **logit** model (**nls**)

![](results/mp_hash_mean_tbl.log_mean.predicted.png)

<br><br>


- with known sub-sampling, we create pseudo OTU profiles, varying the **number of OTUs**
- in double log-space, we see an almost perfect linear relationship

![](results/hash_sd.predicted.png)


<br><br>


- the final _subsampling_ and _#OTU_ had to be tweaked further until the resulting beta diversity profiles matches in within-group mean & SD
- for those, we can then compute power over increasing effect size

<br>

![](results/sim_power_effect_power.png)

