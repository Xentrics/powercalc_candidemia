---
title: "Power Calculation of 16S Candidemia Cohort"
author: "Bastian Seelbinder"
date: "`r {date()}`"
output:
  html_document:
    css: /analysis/src/report.css
    dev: png
    toc: true
    toc_float: true
    df_print: paged
theme: united
---
  
```{r setup, include=FALSE}
knitr::opts_chunk$set(
  echo = FALSE, # do not show code in report
  error = FALSE, # do not interrupt in case of errors
  cache = FALSE, # disable caching to allow external pointers
  root.dir = here::here()
)

options(width = 150)
```


Power calculation of Candidemia cohort, [Zhai et al, Nature medicine 2020](https://doi.org/10.1038/s41591-019-0709-7)

<br><br><br><br>




### Software

**CRAN repository**: `r {getOption("repos") %>% as.character() %>% paste(collapse = ", ")}`

<br>

```{r session-info}
devtools::session_info()
```

---

<br><br><br><br>




# Workflow

```{r drake-workflow, message=FALSE, warning=FALSE, fig.width=14, fig.height=6}
plan_plt <- drake::r_drake_ggraph(
  source = "/analysis/src/01_micropower_make.R",
  targets_only = TRUE,
  label_nodes = TRUE,
  build_times = "none"
)

# draw graph from top to bottom
plan_plt$data$label <-
  plan_plt$data$label %>%
  str_remove("\n\n") %>%
  str_remove("/analysis/results/") %>%
  str_remove("results/") %>%
  str_remove(".*::") %>%
  str_replace_all("\n", " ") %>%
  str_replace("(.*)", "\n\n\\1")

plan_plt

# plan_plt$data$x <-
#   plan_plt$data$x %>%
#   purrr::map_dbl(~ .x * -1)
# 
# plan_plt +
#   ggplot2::coord_flip() +
#   ggplot2::labs(title = "")
```



---

<br><br><br><br>






# HMP Method

- model bacterial profiles using Dirichlet-multinomial distribution (DM)
- with just 15 samples per group, statistical power is greater than 80%
- at 20 samples, power is 100%

<br><br>

![](/analysis/results/HMP_power_DM.png)

---

<br><br><br><br>






# Micropower Method

- this analysis was heavily guided by [this blog](https://medium.com/brown-compbiocore/power-analyses-for-microbiome-studies-with-micropower-8ff28b36dfe3)
- here, the mean within-group and within-SD must be estimated from an existing cohort
- these parameters are then used to create simulated OTU tables _with the same properties_

<br><br><br><br>


- First, rarefaction must be determined
- subsampling influences the within-group **mean**
- Target means were **outside** the simulated data

![](/analysis/results/mp_hash_mean_tbl.png)

<br><br><br>



- in log10 space of mean, we see a **sigmod** curve
- these can be fit well using a **logit** model (**nls**)

![](/analysis/results/mp_hash_mean_tbl.log_mean.predicted.png)

<br><br><br><br>


- with known sub-sampling, we create pseudo OTU profiles, varying the **number of OTUs**
- in double log-space, we see an almost perfect linear relationship

![](/analysis/results/hash_sd.predicted.png)


<br><br><br><br>


- the final _subsampling_ and _#OTU_ had to be tweaked further until the resulting beta diversity profiles matches in within-group mean & SD
- for those, we can then compute power over increasing effect size
- $\Omega^2=0.212$ is enough to achive 80% power using Bray-Kurtis

![](/analysis/results/sim_power_effect_power.png)


<br><br>

- The Candidemia cohort has an $R^2=0.089$, so considerably higher
- This in turn means that power is probably close or equal to 1

```{r}
readxl::read_excel("/analysis/results/candidemia_adonis2.2mean.xlsx", sheet = 1) %>%
  mutate(
    SumOfSqs = round(SumOfSqs, 1),
    statistic = round(statistic, 1),
    R2 = round(R2, 3)
  ) %>%
  flextable::flextable()
```


