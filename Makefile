#!/usr/bin/make -f

# This is the main entry point for the analysis
# All results must be created by making the rule 'analysis'

.PHONY: build run env status analyse

USER_ID := $(shell id -u)
GROUP_ID := $(shell id -g)
PROJECT_ROOT := $(shell pwd)

DOCKER_NAME := $(shell cat raw/.dockername)
# ongoing numbering of env instances
DOCKER_ENV_NR := $(shell docker container ls -f name=^/$(DOCKER_NAME)_env$ | wc -l)
# map env to port 14603 if user has uid 1046 and there are 3 running env distances
DOCKER_ENV_PORT := $(shell echo 1$(shell echo $(USER_ID) | cut -c 3-4)$(shell printf "%02d" $(DOCKER_ENV_NR)))
DOCKER_ENV_NAME := $(DOCKER_NAME)_env_$(DOCKER_ENV_NR)
DOCKER_REGISTRY := sbi-registry.hki-jena.de
DOCKER_REGISTRY_USER := seelbind
DOCKER_NAMESPACE := seelbind
DOCKER_REPOSITORY := $(shell echo $(DOCKER_REGISTRY)/$(DOCKER_NAMESPACE)/$(DOCKER_NAME) | sed 's/\/\//\//g')
DOCKER_TAG := latest
DOCKER_IMAGE := $(DOCKER_REPOSITORY):$(DOCKER_TAG)


define DOCKER_RUN_ARGS
	--volume ${PWD}:/analysis \
	--volume /sbidata:/sbidata \
	--hostname $(DOCKER_NAME)
endef

# Run docker container. This will also run the analysis by default
run:
	docker run --rm \
		--user $(USER_ID):$(GROUP_ID) \
		$(DOCKER_RUN_ARGS) $(DOCKER_IMAGE) make analyse

# Start docker container for interactive analysis
# - detached RStudio Server
# - user rstudio has the same UID than the host user
# - user rstudio as the analysis root directory as home
env:
	docker run -d --rm \
		--user root:root \
		-e PASSWORD=tesseract \
		-p $(DOCKER_ENV_PORT):8787 \
		--name $(DOCKER_ENV_NAME) \
		$(DOCKER_RUN_ARGS) $(DOCKER_IMAGE) \
		bash -c " \
			groupadd -g $(GROUP_ID) GROUP ; \
			usermod -u $(USER_ID) -g $(GROUP_ID) -d /analysis rstudio && \
			env >> /usr/local/lib/R/etc/Renviron && \
			/init" && \
		echo SUCCESS: Interactive analysis container $(DOCKER_ENV_NAME) started at http://$(shell hostname):$(DOCKER_ENV_PORT)

# Status of DVC, git and make
status:
	@echo "----------- DVC status -----------"
	dvc status
	@echo "\n\n----------- git status -----------"
	git status
	@echo "\n\n---------- make status -----------"
	make -n

login:
	docker login \
		-u $(DOCKER_REGISTRY_USER) \
		$(DOCKER_REGISTRY)

pull: 
	docker pull $(DOCKER_IMAGE)

# Build docker image
build:
	docker pull $(DOCKER_IMAGE) || true && \
	docker build \
	 	--tag $(DOCKER_IMAGE) \
		--cache-from $(DOCKER_IMAGE) \
		--build-arg BUILDKIT_INLINE_CACHE=1 \
		$(PROJECT_ROOT) \
		2>&1 | tee log/docker_build.log && \
	docker inspect $(DOCKER_IMAGE) > raw/dockerimage.json && \
	docker push $(DOCKER_IMAGE) || true

# Main entry point for the analysis
analyse: 
	@echo "Start anlysis at ${HOSTNAME}"
	Rscript src/main.R 2>&1 | tee log/analyze.log
