#!/bin/bash

# First, build the docker images
make build

# execute the analysis
make analyse

# or run rstudio-server for further investion
make env
